package com.example.task04;

public class Task04Main {

    public static void main(String[] args) {

        System.out.println(getSeason(10));

    }

    static String getSeason(int monthNumber) {
        String ans2;
        switch (monthNumber) {
            case 12:
            case 1:
            case 2:
                ans2 = "зима";
                break;
            case 3:
            case 4:
            case 5:
                ans2 = "весна";
                break;
            case 6:
            case 7:
            case 8:
                ans2 = "лето";
                break;
            case 9:
            case 10:
            case 11:

                ans2 = "осень";
                break;
            default:
                throw new MyException(
                        String.format("monthNumber %d is invalid, month number should be between 1..12", monthNumber), monthNumber
                );
        }
        return ans2;
    }
}