package com.example.task04;

public class MyException extends IllegalArgumentException {
    private int num;

    public MyException(String message, int num) {
        super(message);
        this.num = num;
    }
}
