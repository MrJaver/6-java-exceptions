package com.example.task02;

public class Task02Main {

    public static void main(String[] args) {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:
        /*
        System.out.println(getSeason(-5));
         */
    }

    static String getSeason(int monthNumber) {
        String ans2;
        switch (monthNumber) {
            case 12:
            case 1:
            case 2:
                ans2 = "зима";
                break;
            case 3:
            case 4:
            case 5:
                ans2 = "весна";
                break;
            case 6:
            case 7:
            case 8:
                ans2 = "лето";
                break;
            case 9:
            case 10:
            case 11:

                ans2 = "осень";
                break;
            default:
                throw new IllegalArgumentException(
                        String.format("monthNumber %d is invalid, month number should be between 1..12", monthNumber)
                );
        }

        return ans2;//todo напишите здесь свою корректную реализацию этого метода, вместо существующей
    }
}